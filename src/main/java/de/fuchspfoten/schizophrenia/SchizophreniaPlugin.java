/*
 * Copyright (c) 2018. All Rights Reserved.
 */

package de.fuchspfoten.schizophrenia;

import de.fuchspfoten.schizophrenia.modules.WorldCommandModule;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Plugin for being loaded by Bukkit.
 */
public class SchizophreniaPlugin extends JavaPlugin {

    /**
     * Singleton plugin instance.
     */
    private @Getter static SchizophreniaPlugin self;

    /**
     * The world manager.
     */
    private @Getter WorldManager worldManager;

    /**
     * Saves the configuration.
     */
    public void save() {
        saveConfig();
    }

    @Override
    public void onEnable() {
        self = this;

        // Create default config.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Create the world manager and load all worlds.
        worldManager = new WorldManager(getConfig().getConfigurationSection("worlds"));
        worldManager.loadAll();

        // Register commands.
        getCommand("world").setExecutor(new WorldCommandModule());

        // Register listeners.
        // ...
    }
}
