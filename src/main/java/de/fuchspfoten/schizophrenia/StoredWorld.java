package de.fuchspfoten.schizophrenia;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bukkit.World.Environment;
import org.bukkit.WorldType;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Represents a stored world.
 */
@RequiredArgsConstructor
@Getter
public class StoredWorld {

    /**
     * Loads a stored world from a {@link org.bukkit.configuration.ConfigurationSection}.
     *
     * @param name    The name of the world.
     * @param section The section to load from.
     * @return The loadded world.
     */
    public static StoredWorld load(final String name, final ConfigurationSection section) {
        return new StoredWorld(name, section.getLong("seed"), Environment.valueOf(section.getString("env")),
                WorldType.valueOf(section.getString("type")), section.getBoolean("structures"),
                section.getString("generator"));
    }

    /**
     * The name of the world.
     */
    private @NonNull final String name;

    /**
     * The seed of the world.
     */
    private final long seed;

    /**
     * The environment of the world.
     */
    private @NonNull final Environment environment;

    /**
     * The type of the world.
     */
    private @NonNull final WorldType type;

    /**
     * The world generates structures iff this is {@code true}.
     */
    private final boolean generatingStructures;

    /**
     * The generator for the world, or empty if none.
     */
    private @NonNull final String generator;

    /**
     * Stores the world in the given {@link org.bukkit.configuration.ConfigurationSection}.
     *
     * @param section The section.
     */
    public void store(final ConfigurationSection section) {
        section.set("seed", seed);
        section.set("env", environment.name());
        section.set("type", type.name());
        section.set("structures", generatingStructures);
        section.set("generator", generator);
    }
}
