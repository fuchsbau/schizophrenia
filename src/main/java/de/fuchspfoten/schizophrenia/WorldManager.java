package de.fuchspfoten.schizophrenia;

import lombok.RequiredArgsConstructor;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Manages worlds.
 */
@RequiredArgsConstructor
public class WorldManager {

    /**
     * Loads or creates (if not existing) a world.
     *
     * @param world The world to load or create.
     */
    private static void loadOrCreateWorld(final StoredWorld world) {
        final WorldCreator creator = new WorldCreator(world.getName())
                .environment(world.getEnvironment())
                .seed(world.getSeed())
                .type(world.getType())
                .generateStructures(world.isGeneratingStructures());
        if (!world.getGenerator().isEmpty()) {
            creator.generator(world.getGenerator());
        }
        creator.createWorld();
    }

    /**
     * The section in which worlds are stored.
     */
    private final ConfigurationSection worldsSection;

    /**
     * Loads all known worlds.
     */
    public void loadAll() {
        for (final String key : worldsSection.getKeys(false)) {
            final StoredWorld stored = StoredWorld.load(key, worldsSection.getConfigurationSection(key));
            loadOrCreateWorld(stored);
        }
    }
}
