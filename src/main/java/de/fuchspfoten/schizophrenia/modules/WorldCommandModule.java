package de.fuchspfoten.schizophrenia.modules;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.stream.Collectors;

/**
 * /world manages worlds. This is not a tree command as we cannot use FuchsLib here.
 */
public class WorldCommandModule implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label,
                             final String[] args) {
        if (!sender.hasPermission("schizophrenia.manage")) {
            sender.sendMessage("Missing permission: schizophrenia.manage");
            return true;
        }

        if (args.length == 0) {
            sender.sendMessage("/world list -- Lists worlds.");
        } else if (args.length == 1 && args[0].equals("list")) {
            sender.sendMessage("Worlds: " + String.join(", ", Bukkit.getWorlds().stream()
                    .map(World::getName)
                    .collect(Collectors.toList())));
        } else {
            sender.sendMessage("Invalid command. Try /world.");
        }

        return true;
    }
}
